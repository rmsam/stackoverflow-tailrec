package redbook

import scala.annotation.tailrec

enum List[+A] {
  case Cons(head :A, tail: List[A])
  case Nil extends List[Nothing]
}

object List {  
  @tailrec
  def drop[A](n: Int, as: List[A]): List[A] = 
    (n,as) match
      case (0, _) => as
      case (_, Nil) => Nil
      case (x, Cons(_, tail)) => drop(x-1, tail)
}